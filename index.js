const express = require('express')
const request = require('request')
const bodyParser = require('body-parser')
const app = express()
const port = process.env['PORT'] || 3000
const token = process.env['TOKEN'] 
const integrationKey = process.env['INTEGRATION_KEY'] 

function incident(from, text, cb) {
  request({
    method: 'POST',
    uri: 'https://events.pagerduty.com/v2/enqueue',
    json: true,
    body: {
      routing_key: integrationKey,
      event_action: 'trigger',
      payload: {
        summary: "From: " + from + " - " + text,
        source: 'app.doublegdp.com',
        severity: 'warning',
      }
    }
  }, cb)
}

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.get('/alert/:token', (req, res) => {
  console.log(req.query)
  console.log(req.body)
  console.log(req.params)
  if (token && token === req.params.token) {
    console.log("Firing incident", req.query.msisdn, req.query.text)
    incident(req.query.msisdn, req.query.text, () => {
      return res.status(200).send('OK')
    })
  } else {
    return res.status(500).send('Fail')
  }
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
